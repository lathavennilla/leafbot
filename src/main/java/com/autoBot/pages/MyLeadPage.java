package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadPage extends Annotations{

	public MyLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
@FindBy(how=How.LINK_TEXT,using="Create Lead")WebElement eleCreateLeadLink;
@FindBy(how=How.LINK_TEXT,using="Find Leads")WebElement eleFindLeadsLink;
@FindBy(how=How.LINK_TEXT,using="Merge Leads")WebElement eleMergeLeadsLink;


	public CreateLeadPage clickCreateLeadLink() {
		click(eleCreateLeadLink);
		//eleCreateLeadLink.click();
		return new CreateLeadPage();
	}


	/*public Object clickLead() {
		// TODO Auto-generated method stub
		return null;
	}*/

	/*public FindLeadsPage clickFindLeadsLink() {
		eleFindLeadsLink.click();
		return new FindLeadsPage();	
	}
	public MergeLeadsPage clickMergeLeadsLink() {
		eleMergeLeadsLink.click();
		return new MergeLeadsPage();
	}	*/
}
