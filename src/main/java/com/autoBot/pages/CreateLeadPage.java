package com.autoBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

@FindBy(how=How.ID,using="createLeadForm_companyName")WebElement eleCompanyName;
@FindBy(how=How.ID,using="createLeadForm_firstName")WebElement eleFirstName;
@FindBy(how=How.ID,using="createLeadForm_lastName")WebElement eleLastName;
@FindBy(how=How.XPATH,using="//input[@name='submitButton']")WebElement eleSubmit;

	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(eleCompanyName,data);
		//eleCompanyName.sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String data) {	
		clearAndType(eleFirstName,data);
		//eleFirstName.sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastName,data);
		//eleLastName.sendKeys(data);		
		return this;
	}
	
	public ViewLeadPage clickCreateLeadButton() {
		click(eleSubmit);
		//eleSubmit.click();
		return new ViewLeadPage();		
	}
}
